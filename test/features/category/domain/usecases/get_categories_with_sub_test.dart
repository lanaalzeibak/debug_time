import 'package:dartz/dartz.dart';
import 'package:debugging_time/core/usecases/usecases.dart';
import 'package:debugging_time/features/category/domain/entities/category_entity.dart';
import 'package:debugging_time/features/category/domain/repositories/categories_reposotory.dart';
import 'package:debugging_time/features/category/domain/usecases/get_categories_with_sub.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockGetCategorySubRepository extends Mock
    implements CategoriesRepository {}

void main() {
  GetAllCategoriesWithSub usecase;
  MockGetCategorySubRepository mockCategoriesRepository;

  setUp(() {
    mockCategoriesRepository = MockGetCategorySubRepository();
    usecase = GetAllCategoriesWithSub(mockCategoriesRepository);
  });

  final entities = CategoriesEntity(
      categoryId: "2",
      color: "#ffffff",
      isHidden: false,
      name: "test",
      parentId: "1");

  test(
    'tttest should get all categories with sub category',
    () async {
      // arrange
      when(mockCategoriesRepository.getAllCategoriesWithSubs())
          .thenAnswer((_) async => Right(entities));
      // act
      final result = await usecase(NoParams());
      // assert
      expect(result, Right(entities));
      verify(mockCategoriesRepository.getAllCategoriesWithSubs());
      verifyNoMoreInteractions(mockCategoriesRepository);
    },
  );
}
