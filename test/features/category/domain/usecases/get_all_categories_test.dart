import 'package:dartz/dartz.dart';
import 'package:debugging_time/core/usecases/usecases.dart';
import 'package:debugging_time/features/category/domain/entities/category_entity.dart';
import 'package:debugging_time/features/category/domain/repositories/categories_reposotory.dart';
import 'package:debugging_time/features/category/domain/usecases/get_all_categories.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockGetCategoryRepository extends Mock implements CategoriesRepository {}

void main() {
  GetAllCategories usecase;
  MockGetCategoryRepository mockCategoriesRepository;

  setUp(() {
    mockCategoriesRepository = MockGetCategoryRepository();
    usecase = GetAllCategories(mockCategoriesRepository);
  });

  final entities = CategoriesEntity(
      categoryId: "1",
      color: "#ffffff",
      isHidden: false,
      name: "test",
      parentId: "0");

  test(
    'tttest should get all categories list',
    () async {
      // arrange
      when(mockCategoriesRepository.getAllCategories())
          .thenAnswer((_) async => Right(entities));
      // act
      final result = await usecase(NoParams());
      // assert
      expect(result, Right(entities));
      verify(mockCategoriesRepository.getAllCategories());
      verifyNoMoreInteractions(mockCategoriesRepository);
    },
  );
}
