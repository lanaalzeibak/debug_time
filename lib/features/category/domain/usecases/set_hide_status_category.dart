import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../entities/category_entity.dart';
import '../repositories/categories_reposotory.dart';

class SetHideStatusCategory
    implements UseCase<CategoriesEntity, SetHideParams> {
  final CategoriesRepository repository;

  SetHideStatusCategory(this.repository);

  @override
  Future<Either<Failure, CategoriesEntity>> call(SetHideParams params) async {
    return await repository.setHideStatusCategory(
      categoryId: params.categoryId,
      isHide: params.isHide,
    );
  }
}

class SetHideParams {
  final String categoryId;
  final bool isHide;

  SetHideParams({
    @required this.categoryId,
    @required this.isHide,
  });
}
