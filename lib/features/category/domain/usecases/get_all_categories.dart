import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../entities/category_entity.dart';
import '../repositories/categories_reposotory.dart';

class GetAllCategories implements UseCase<CategoriesEntity, NoParams> {
  final CategoriesRepository repository;

  GetAllCategories(this.repository);

  @override
  Future<Either<Failure, CategoriesEntity>> call(NoParams params) async {
    return await repository.getAllCategories();
  }
}
