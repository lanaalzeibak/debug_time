import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class CategoriesEntity extends Equatable {
  final String categoryId;
  final String name;
  final String parentId; // 0 when no parent
  final String color;
  final bool isHidden;

  CategoriesEntity({
    @required this.categoryId,
    @required this.name,
    @required this.parentId,
    @required this.color,
    @required this.isHidden,
  });

  @override
  List<Object> get props => [
        categoryId,
        name,
        parentId,
        color,
        isHidden,
        categoryId,
      ];
}
