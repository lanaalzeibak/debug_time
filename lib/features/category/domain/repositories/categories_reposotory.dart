import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/errors/failures.dart';
import '../entities/category_entity.dart';

abstract class CategoriesRepository {
  Future<Either<Failure, CategoriesEntity>> getAllCategories();
  Future<Either<Failure, CategoriesEntity>> getAllCategoriesWithSubs();
  Future<Either<Failure, CategoriesEntity>> setHideStatusCategory(
      {@required String categoryId, @required bool isHide});
}
